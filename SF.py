# -*- coding: utf-8 -*-
'''
1. Данный скрипт тестировался на Python v3.6 и  MySQL Server version  5.6
2. Были установленны две библиотеки из файла requirements.txt (pip install -r requirements.txt)
3. Две задачи были объеденены в один скрипт
4. Из условия задачи 2 неочевидно какие именно (обработанные или нет) и каким образом данные должны быть записаны в БД.
    Способ записи был выбран субъективным образом.

p.s. По каждому пункту готов дать развернутый комментарий.
'''

import json
import time
import requests
import mysql.connector
from mysql.connector import Error

SQL_OPTIONS = {
    "host": "localhost",
    "user": "bm_user",
    "passwd": "qw12er34",
    "database": "dbSkillFactory",
    "table_name": "course_structure",
    "column_name_prefix": "m_"
}


class ExampleETL:
    INDENT_CODE = ('\u0020')
    URL = "http://analytics.skillfactory.ru:5000/api/v1.0.1/get_structure_course/"

    def __init__(self):
        self.has_already = []
        self.blocks = None
        self.cursor = None
        self.database = SQL_OPTIONS['database'].lower()
        self.table = SQL_OPTIONS['table_name'].lower()
        self.prefix = SQL_OPTIONS['column_name_prefix']
        self.start_time = time.time()

    def get_data(self):
        print("Extracting from %s ..." % ExampleETL.URL)
        response = requests.post(url=ExampleETL.URL)
        if response.status_code == 200:
            # response.encoding = 'utf-8'  # Optional: requests infers this internally
            json_response = response.json()
            blocks = json_response['blocks']
            # print(json.dumps(json_response, sort_keys=False, indent=4))
            return blocks
        elif response.status_code == 404:
            print('Error, failed to get data!')
            return False

    def find_children(self, modules, initial_indent):
        children = modules.get("children", 0)

        print("{} {} - {}".format(initial_indent, modules['display_name'], modules['id']))
        initial_indent += ExampleETL.INDENT_CODE * 4
        self.has_already.append(modules['id'])
        if children:
            for child in children:
                if child in self.blocks:
                    self.find_children(modules=self.blocks[child], initial_indent=initial_indent)

    def readable_view(self):
        print("Getting Ready to Print Readable Records:")
        if self.blocks is not None:
            for key, modules in self.blocks.items():
                children = modules.get("children", 0)
                if children and key not in self.has_already:
                    print("\n\n")
                    self.find_children(modules=modules, initial_indent=ExampleETL.INDENT_CODE)

    def create_database(self) -> bool:
        self.cursor.execute("SHOW DATABASES LIKE '{}' ".format(self.database))
        result = self.cursor.fetchone()
        if result:
            print("Is database %s" % self.database)
        else:
            print("No database %s, CREATE IT" % self.database)
            try:
                self.cursor.execute(
                    "CREATE DATABASE {} CHARACTER SET utf8 COLLATE utf8_general_ci".format(self.database))
            except Error as e:
                print("Error while create database", e)
                return False
        return True

    def req_is_table(self) -> bool:
        self.cursor.execute("SHOW TABLES LIKE '{}'".format(self.table))
        result = self.cursor.fetchone()
        if result:
            return True
        else:
            return False

    def req_is_column(self, column) -> bool:
        if self.prefix is not None:
            column = self.prefix + column
        self.cursor.execute("SHOW COLUMNS FROM {0} LIKE '{1}'".format(self.table, column))
        result = self.cursor.fetchone()

        if result:
            return True
        else:
            return False

    def create_table(self) -> bool:
        result = self.req_is_table()
        if result:
            print("There is a table %s, DELETE AND CREATE IT" % self.table)
            try:
                self.cursor.execute("DROP TABLE {}".format(self.table))
            except Error as e:
                print("Error while delete table", e)
                return False

        else:
            print("No table %s, CREATE IT" % self.table)

        try:
            query = "CREATE TABLE `{}` (" \
                    "`id` INT(11) NOT NULL AUTO_INCREMENT," \
                    "`identity` VARCHAR(250)," \
                    "`update_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP," \
                    "PRIMARY KEY (`id`));".format(self.table)
            self.cursor.execute(query)
        except Error as e:
            print("Error while create table", e)
            return False
        return True

    def record(self, connection) -> bool:
        len_blocks = len(self.blocks)
        print("Total {} records".format(len_blocks))
        print("Disassemble and write data to the {}".format(self.table))
        i = 0
        percent_text = 'Recording in progress {} %'
        for key, modules in self.blocks.items():
            i += 1
            percent_val = int((i / len_blocks) * 100)
            print(percent_text.format(percent_val) + '\r', end='')
            columns_name = []
            columns_value = []
            columns_name.append('identity')
            columns_value.append(key)
            for m_key, m_val in modules.items():
                columns_name.append(self.prefix + m_key)
                columns_value.append(m_val)
                if not self.req_is_column(m_key):
                    column_name = self.prefix + m_key
                    if m_key == "children":
                        column_type = "LONGTEXT NULL DEFAULT NULL"
                    else:
                        column_type = "VARCHAR(255) NULL DEFAULT NULL"
                    query = "ALTER TABLE `{0}` ADD COLUMN `{1}` {2};".format(self.table, column_name, column_type)
                    try:
                        self.cursor.execute(query)
                    except Error as e:
                        print("Error while ADD COLUMN in table", e)

            columns_name = ', '.join(["`%s`" % i for i in columns_name])
            columns_value = ', '.join(['"%s"' % i for i in columns_value])
            query = "INSERT INTO {0} ({1}) VALUES ({2});".format(self.table, columns_name, columns_value)
            try:
                self.cursor.execute(query)
                connection.commit()
            except Error as e:
                print("Error while records in table", e)
        return True

    def connect(self):
        try:
            connection = mysql.connector.connect(
                host=SQL_OPTIONS["host"],
                user=SQL_OPTIONS["user"],
                passwd=SQL_OPTIONS["passwd"]
            )
            if connection.is_connected():
                db_Info = connection.get_server_info()
                print("Connected to MySQL Server version ", db_Info)
                self.cursor = connection.cursor()
                self.create_database()
                connection = mysql.connector.connect(
                    host=SQL_OPTIONS["host"],
                    user=SQL_OPTIONS["user"],
                    passwd=SQL_OPTIONS["passwd"],
                    database=self.database
                )
                self.cursor = connection.cursor()
                self.create_table()
                self.record(connection=connection)


        except Error as e:
            print("Error while connecting to MySQL", e)

        finally:
            print("ALL DATA HAS BEEN RECORDED!")
            if (connection.is_connected()):
                self.cursor.close()
                connection.close()
                print("MySQL connection is closed")

    def run(self):
        self.blocks = self.get_data()
        self.readable_view()
        self.connect()
        print("\nScript execution time \n----- %s sec. -----" % (int(time.time() - self.start_time)))


ExampleETL().run()
